import { ss } from '@/utils/storage'
import { t } from '@/locales'
const LOCAL_NAME = 'userStorage-midjourney'

export interface UserInfo {
  avatar: string
  name: string
  description: string
}

export interface UserState {
  userInfo: UserInfo
}

export function defaultSetting(): UserState {
  return {
    userInfo: {
      avatar: 'https://static.yl.nbwbw.com/uploads/activity/logo/znmt_logo_3.png',
      name:  t('mjset.sysname'),//'AI绘图',
      description: 'Star on <a href="https://github.com/Dooy/chatgpt-web-midjourney-proxy" class="text-blue-500" target="_blank" >GitHub</a>',
    },
  }
}

export function getLocalState(): UserState {
  const localSetting: UserState | undefined = ss.get(LOCAL_NAME)
	const localValue = { ...defaultSetting(), ...localSetting };
	localValue.userInfo.avatar = 'https://static.yl.nbwbw.com/uploads/activity/logo/znmt_logo_2.jpg';
	// console.log('getLocalState', localValue)
  return localValue;
}

export function setLocalState(setting: UserState): void {
  ss.set(LOCAL_NAME, setting)
}
